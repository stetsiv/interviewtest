
import org.testng.Assert;
import org.testng.annotations.Test;
import pageobjects.CalculationPage;


/**
 * Created by Yura on 17.06.2017.
 */
public class SliderTest extends BaseTest {
   public final static int PRICE_PURCHASE = 2000000;

   @Test
    public void testSlider()  {
       Assert.assertEquals(PRICE_PURCHASE,new CalculationPage().moveSliderToRight(),"Slider was not moved");
   }

}
