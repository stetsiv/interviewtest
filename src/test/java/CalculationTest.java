import org.testng.Assert;
import org.testng.annotations.Test;
import pageobjects.MainPage;
import pageobjects.LoanPage;
import pageobjects.MortgagePage;
import pageobjects.CalculationPage;

/**
 * Created by Yura on 18.06.2017.
 */
public class CalculationTest extends  BaseTest {

    public final static int PRICE_PURCHASE = 500000;
    public final static int DOWN_PAYMENT= 100000;
    public final static String YEARS= "15";
    public final static String FREQUENCY= "weekly";
    public final static double INTEREST_RATE= 5.0;
    public final static String MORTGAGE_CALCULATED_VALUE= "$ 726.35";



    @Test
    public void testValue() {
        CalculationPage mpcp = new CalculationPage();
        mpcp.calculate(PRICE_PURCHASE,DOWN_PAYMENT,YEARS,FREQUENCY,INTEREST_RATE);
        Assert.assertEquals( mpcp.getCalculatedValue(),MORTGAGE_CALCULATED_VALUE,"Mortgage value is not correct");
    }
}
