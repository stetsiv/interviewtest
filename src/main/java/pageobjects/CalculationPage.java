package pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import tools.BrowserDriver;
import webelements.calculationpage.*;

/**
 * Created by Yura on 18.06.2017.
 */
public class CalculationPage {


    public CalculationPage() {
        PageFactory.initElements(BrowserDriver.getInstance(), this);
    }

    @FindBy(xpath = "//div[contains(@class,'slider-track-high')]")
    WebElement slider;

    @FindBy(id = "sliderPrixPropriete")
    WebElement priceBox;

    @FindBy(id = "PrixProprietePlus")
    WebElement purchasePricePlusButton;

    @FindBy(id = "MiseDeFondPlus")
    WebElement downPaymentPlusButton;

    @FindBy(xpath = ".//*[@id='form_calculateur_versements']/div[4]/div[1]/div/div[2]/p")
    WebElement amortisationDropDawn;

    @FindBy(xpath = ".//*[@id='form_calculateur_versements']/div[4]/div[1]/div/div[3]/div/ul")
    WebElement amortisationOptionList;

    @FindBy(xpath = ".//*[@id='form_calculateur_versements']/div[5]/div[2]/p")
    WebElement paymentFrequencyDropDawn;

    @FindBy(xpath = ".//*[@id='form_calculateur_versements']/div[5]/div[3]/div/ul")
    WebElement paymentFrequencyOptionList;

    @FindBy(id = "TauxInteret")
    WebElement interestRateInputField;

    @FindBy(id = "btn_calculer")
    WebElement calculateButton;

    @FindBy(id = "paiement-resultats")
    WebElement mortgageResultFiled;


    public int moveSliderToRight() {

        int width = slider.getSize().getWidth();
        int locationX = slider.getLocation().getX();

        Actions builder = new Actions(BrowserDriver.getInstance());
        builder.moveToElement(slider)
                .click()
                .dragAndDropBy(slider, locationX + width, 0)
                .build()
                .perform();
        int purchasePrice = Integer.parseInt(priceBox.getAttribute("value"));
        return purchasePrice;
    }

    public void calculate(int price, int payment, String years, String frequency, double rate) {
        new PurchasePricePlusButton(purchasePricePlusButton, price).click();
        new DownPaymentPlusButton(downPaymentPlusButton, payment).click();
        new AmortisationDropDown(amortisationDropDawn, amortisationOptionList).selectAmortisation(years);
        new PaymentFrequencyDropDown(paymentFrequencyDropDawn, paymentFrequencyOptionList).selectPaymentFrequency(frequency);
        new InputInterestRateField(interestRateInputField).typeRate(rate);
        new CalculateButton(calculateButton).click();
    }


    public String getCalculatedValue() {
        new WebDriverWait(BrowserDriver.getInstance(), 30)
                .until(ExpectedConditions.visibilityOf(mortgageResultFiled));
        return new MortgageResultField(mortgageResultFiled).getMortgageCalculatedValue();
    }
}



