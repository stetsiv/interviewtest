package pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import tools.BrowserDriver;
import webelements.mortagepage.CalculatePayButton;

/**
 * Created by Yura on 18.06.2017.
 */
public class MortgagePage {

    public MortgagePage() {
        PageFactory.initElements(BrowserDriver.getInstance(), this);
    }

    @FindBy(xpath ="//a[contains(text(), 'Calculate your payments')]" )
    WebElement calculateYourPaymentsButton;

    public CalculationPage goToPage4() {
        new WebDriverWait(BrowserDriver.getInstance(), 10)
                .until(ExpectedConditions.visibilityOf(calculateYourPaymentsButton));
        new CalculatePayButton(calculateYourPaymentsButton).click();
        return new CalculationPage();
    }


}
