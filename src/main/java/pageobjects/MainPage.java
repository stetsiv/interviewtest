package pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import tools.BrowserDriver;
import webelements.mainpage.LoansTab;

/**
 * Created by Yura on 18.06.2017.
 */
public class MainPage {

    public MainPage() {
        PageFactory.initElements(BrowserDriver.getInstance(), this);
    }

    @FindBy(xpath ="//span[contains(text(), 'Loans'  )]" )
    private WebElement loansButton;

    public LoanPage goToPage2() {
         new WebDriverWait(BrowserDriver.getInstance(), 10)
                .until(ExpectedConditions.visibilityOf(loansButton));
         new LoansTab(loansButton).click();
        return new LoanPage();
    }

}
