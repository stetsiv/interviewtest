package pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import tools.BrowserDriver;
import webelements.loanpage.MortgageLink;

/**
 * Created by Yura on 18.06.2017.
 */
public class LoanPage {


    public LoanPage() {
        PageFactory.initElements(BrowserDriver.getInstance(), this);
    }

    @FindBy(xpath =".//a[contains(text(),'Mortgages')]" )
    WebElement mortageButton;

    public MortgagePage goToPage3() {
        new WebDriverWait(BrowserDriver.getInstance(), 10)
                .until(ExpectedConditions.visibilityOf(mortageButton));
        new MortgageLink(mortageButton).click();
        return new MortgagePage();
    }
}
