package webelements.calculationpage;

import org.openqa.selenium.WebElement;

/**
 * Created by Yura on 18.06.2017.
 */
public class DownPaymentPlusButton {
    private WebElement element;
    private int payment;
    public final static int MAX_SLIDER_PAYMENT = 10000000;
    public final static int SLIDER_STEP = 100000;

    public DownPaymentPlusButton(WebElement element, int payment) {
        this.element = element;
        this.payment = payment;
    }

    public void click() {
        for (int i = 0; i < MAX_SLIDER_PAYMENT; i += SLIDER_STEP) {
            if (i != payment) {
                this.element.click();
            } else {
                break;
            }
        }
    }
}
