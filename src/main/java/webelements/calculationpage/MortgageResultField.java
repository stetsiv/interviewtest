package webelements.calculationpage;

import org.openqa.selenium.WebElement;

/**
 * Created by Yura on 19.06.2017.
 */
public class MortgageResultField {
    private WebElement element;

    public MortgageResultField(WebElement element) {
        this.element = element;
    }

    public String getMortgageCalculatedValue() {
        return this.element.getText().toString();
    }
}
