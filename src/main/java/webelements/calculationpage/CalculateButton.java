package webelements.calculationpage;

import org.openqa.selenium.WebElement;

/**
 * Created by Yura on 19.06.2017.
 */
public class CalculateButton {
    private WebElement element;

    public CalculateButton(WebElement element) {
        this.element = element;
    }

    public void click() {
        this.element.click();
    }
}
