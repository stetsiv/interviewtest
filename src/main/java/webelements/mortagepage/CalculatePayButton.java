package webelements.mortagepage;

import org.openqa.selenium.WebElement;

/**
 * Created by Yura on 19.06.2017.
 */
public class CalculatePayButton {
    private WebElement element;

    public CalculatePayButton(WebElement element) {
        this.element = element;
    }

    public void click(){
        this.element.click();
    }
}
